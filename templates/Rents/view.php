<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rent $rent
 */
?>

<?php
$this->assign('title', __('Rent'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Rents', 'url' => ['action' => 'index']],
    ['title' => 'View'],
]);
?>

<div class="view card card-primary card-outline">
  <div class="card-header d-sm-flex">
    <h2 class="card-title"><?= h($rent->id_rent) ?></h2>
  </div>
  <div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <tr>
            <th><?= __('Film') ?></th>
            <td><?= $rent->has('film') ? $this->Html->link($rent->film->name, ['controller' => 'Films', 'action' => 'view', $rent->film->id_film]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id Rent') ?></th>
            <td><?= $this->Number->format($rent->id_rent) ?></td>
        </tr>
        <tr>
            <th><?= __('Rent Date') ?></th>
            <td><?= h($rent->rent_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Return Date') ?></th>
            <td><?= h($rent->return_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($rent->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($rent->modified) ?></td>
        </tr>
    </table>
  </div>
  <div class="card-footer d-flex">
    <div class="">
      <?= $this->Form->postLink(
          __('Delete'),
          ['action' => 'delete', $rent->id_rent],
          ['confirm' => __('Are you sure you want to delete # {0}?', $rent->id_rent), 'class' => 'btn btn-danger']
      ) ?>
    </div>
    <div class="ml-auto">
      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $rent->id_rent], ['class' => 'btn btn-secondary']) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>
</div>


