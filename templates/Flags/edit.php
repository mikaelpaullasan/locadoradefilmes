<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Flag $flag
 */
?>
<?php
$this->assign('title', __('Edit Flag'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Flags', 'url' => ['action' => 'index']],
    ['title' => 'View', 'url' => ['action' => 'view', $flag->id_flag]],
    ['title' => 'Edit'],
]);
?>

<div class="card card-primary card-outline">
  <?= $this->Form->create($flag) ?>
  <div class="card-body">
    <?php
      echo $this->Form->control('flag');
      echo $this->Form->control('price');
    ?>
  </div>

  <div class="card-footer d-flex">
    <div class="">
      <?= $this->Form->postLink(
          __('Delete'),
          ['action' => 'delete', $flag->id_flag],
          ['confirm' => __('Are you sure you want to delete # {0}?', $flag->id_flag), 'class' => 'btn btn-danger']
      ) ?>
    </div>
    <div class="ml-auto">
      <?= $this->Form->button(__('Save')) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>

  <?= $this->Form->end() ?>
</div>

