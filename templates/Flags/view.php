<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Flag $flag
 */
?>

<?php
$this->assign('title', __('Flag'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Flags', 'url' => ['action' => 'index']],
    ['title' => 'View'],
]);
?>

<div class="view card card-primary card-outline">
  <div class="card-header d-sm-flex">
    <h2 class="card-title"><?= h($flag->flag) ?></h2>
  </div>
  <div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <tr>
            <th><?= __('Flag') ?></th>
            <td><?= h($flag->flag) ?></td>
        </tr>
        <tr>
            <th><?= __('Id Flag') ?></th>
            <td><?= $this->Number->format($flag->id_flag) ?></td>
        </tr>
        <tr>
            <th><?= __('Price') ?></th>
            <td><?= $this->Number->format($flag->price) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($flag->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($flag->modified) ?></td>
        </tr>
    </table>
  </div>
  <div class="card-footer d-flex">
    <div class="">
      <?= $this->Form->postLink(
          __('Delete'),
          ['action' => 'delete', $flag->id_flag],
          ['confirm' => __('Are you sure you want to delete # {0}?', $flag->id_flag), 'class' => 'btn btn-danger']
      ) ?>
    </div>
    <div class="ml-auto">
      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $flag->id_flag], ['class' => 'btn btn-secondary']) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>
</div>


<div class="related related-films view card">
  <div class="card-header d-sm-flex">
    <h3 class="card-title"><?= __('Related Films') ?></h3>
    <div class="card-toolbox">
      <?= $this->Html->link(__('New'), ['controller' => 'Films' , 'action' => 'add'], ['class' => 'btn btn-primary btn-sm']) ?>
      <?= $this->Html->link(__('List '), ['controller' => 'Films' , 'action' => 'index'], ['class' => 'btn btn-primary btn-sm']) ?>
    </div>
  </div>
  <div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
      <tr>
          <th><?= __('Id Film') ?></th>
          <th><?= __('Name') ?></th>
          <th><?= __('Description') ?></th>
          <th><?= __('Duration') ?></th>
          <th><?= __('Flag Id') ?></th>
          <th><?= __('Type Id') ?></th>
          <th><?= __('Created') ?></th>
          <th><?= __('Modified') ?></th>
          <th class="actions"><?= __('Actions') ?></th>
      </tr>
      <?php if (empty($flag->films)) { ?>
        <tr>
            <td colspan="9" class="text-muted">
              Films record not found!
            </td>
        </tr>
      <?php }else{ ?>
        <?php foreach ($flag->films as $films) : ?>
        <tr>
            <td><?= h($films->id_film) ?></td>
            <td><?= h($films->name) ?></td>
            <td><?= h($films->description) ?></td>
            <td><?= h($films->duration) ?></td>
            <td><?= h($films->flag_id) ?></td>
            <td><?= h($films->type_id) ?></td>
            <td><?= h($films->created) ?></td>
            <td><?= h($films->modified) ?></td>
            <td class="actions">
              <?= $this->Html->link(__('View'), ['controller' => 'Films', 'action' => 'view', $films->id_film], ['class'=>'btn btn-xs btn-outline-primary']) ?>
              <?= $this->Html->link(__('Edit'), ['controller' => 'Films', 'action' => 'edit', $films->id_film], ['class'=>'btn btn-xs btn-outline-primary']) ?>
              <?= $this->Form->postLink(__('Delete'), ['controller' => 'Films', 'action' => 'delete', $films->id_film], ['class'=>'btn btn-xs btn-outline-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $films->id_film)]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
      <?php } ?>
    </table>
  </div>
</div>

