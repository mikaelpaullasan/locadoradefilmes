<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Flag $flag
 */
?>
<?php
$this->assign('title', __('Add Flag'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Flags', 'url' => ['action' => 'index']],
    ['title' => 'Add'],
]);
?>

<div class="card card-primary card-outline">
  <?= $this->Form->create($flag) ?>
  <div class="card-body">
    <?php
      echo $this->Form->control('flag');
      echo $this->Form->control('price');
    ?>
  </div>

  <div class="card-footer d-flex">
    <div class="ml-auto">
      <?= $this->Form->button(__('Save')) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>

  <?= $this->Form->end() ?>
</div>

