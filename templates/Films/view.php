<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Film $film
 */
?>

<?php
$this->assign('title', __('Film'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Films', 'url' => ['action' => 'index']],
    ['title' => 'View'],
]);
?>

<div class="view card card-primary card-outline">
  <div class="card-header d-sm-flex">
    <h2 class="card-title"><?= h($film->name) ?></h2>
  </div>
  <div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($film->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Flag') ?></th>
            <td><?= $film->has('flag') ? $this->Html->link($film->flag->flag, ['controller' => 'Flags', 'action' => 'view', $film->flag->id_flag]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Type') ?></th>
            <td><?= $film->has('type') ? $this->Html->link($film->type->nome, ['controller' => 'Types', 'action' => 'view', $film->type->id_type]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id Film') ?></th>
            <td><?= $this->Number->format($film->id_film) ?></td>
        </tr>
        <tr>
            <th><?= __('Duration') ?></th>
            <td><?= h($film->duration) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($film->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($film->modified) ?></td>
        </tr>
    </table>
  </div>
  <div class="card-footer d-flex">
    <div class="">
      <?= $this->Form->postLink(
          __('Delete'),
          ['action' => 'delete', $film->id_film],
          ['confirm' => __('Are you sure you want to delete # {0}?', $film->id_film), 'class' => 'btn btn-danger']
      ) ?>
    </div>
    <div class="ml-auto">
      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $film->id_film], ['class' => 'btn btn-secondary']) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>
</div>

<div class="view text card">
  <div class="card-header">
    <h3 class="card-title"><?= __('Description') ?></h3>
  </div>
  <div class="card-body">
    <?= $this->Text->autoParagraph(h($film->description)); ?>
  </div>
</div>

<div class="related related-rents view card">
  <div class="card-header d-sm-flex">
    <h3 class="card-title"><?= __('Related Rents') ?></h3>
    <div class="card-toolbox">
      <?= $this->Html->link(__('New'), ['controller' => 'Rents' , 'action' => 'add'], ['class' => 'btn btn-primary btn-sm']) ?>
      <?= $this->Html->link(__('List '), ['controller' => 'Rents' , 'action' => 'index'], ['class' => 'btn btn-primary btn-sm']) ?>
    </div>
  </div>
  <div class="card-body table-responsive p-0">
    <table class="table table-hover text-nowrap">
      <tr>
          <th><?= __('Id Rent') ?></th>
          <th><?= __('Film Id') ?></th>
          <th><?= __('Rent Date') ?></th>
          <th><?= __('Return Date') ?></th>
          <th><?= __('Created') ?></th>
          <th><?= __('Modified') ?></th>
          <th class="actions"><?= __('Actions') ?></th>
      </tr>
      <?php if (empty($film->rents)) { ?>
        <tr>
            <td colspan="7" class="text-muted">
              Rents record not found!
            </td>
        </tr>
      <?php }else{ ?>
        <?php foreach ($film->rents as $rents) : ?>
        <tr>
            <td><?= h($rents->id_rent) ?></td>
            <td><?= h($rents->film_id) ?></td>
            <td><?= h($rents->rent_date) ?></td>
            <td><?= h($rents->return_date) ?></td>
            <td><?= h($rents->created) ?></td>
            <td><?= h($rents->modified) ?></td>
            <td class="actions">
              <?= $this->Html->link(__('View'), ['controller' => 'Rents', 'action' => 'view', $rents->id_rent], ['class'=>'btn btn-xs btn-outline-primary']) ?>
              <?= $this->Html->link(__('Edit'), ['controller' => 'Rents', 'action' => 'edit', $rents->id_rent], ['class'=>'btn btn-xs btn-outline-primary']) ?>
              <?= $this->Form->postLink(__('Delete'), ['controller' => 'Rents', 'action' => 'delete', $rents->id_rent], ['class'=>'btn btn-xs btn-outline-danger', 'confirm' => __('Are you sure you want to delete # {0}?', $rents->id_rent)]) ?>
            </td>
        </tr>
        <?php endforeach; ?>
      <?php } ?>
    </table>
  </div>
</div>

