<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Film $film
 */
?>
<?php
$this->assign('title', __('Add Film'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Films', 'url' => ['action' => 'index']],
    ['title' => 'Add'],
]);
?>

<div class="card card-primary card-outline">
  <?= $this->Form->create($film) ?>
  <div class="card-body">
    <?php
      echo $this->Form->control('name');
      echo $this->Form->control('description');
      echo $this->Form->control('duration', ['empty' => true]);
      echo $this->Form->control('flag_id', ['options' => $flags, 'empty' => true]);
      echo $this->Form->control('type_id', ['options' => $types, 'empty' => true]);
    ?>
  </div>

  <div class="card-footer d-flex">
    <div class="ml-auto">
      <?= $this->Form->button(__('Save')) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>

  <?= $this->Form->end() ?>
</div>

