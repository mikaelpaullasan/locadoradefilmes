<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Film $film
 */
?>
<?php
$this->assign('title', __('Edit Film'));
$this->Breadcrumbs->add([
    ['title' => 'Home', 'url' => '/'],
    ['title' => 'List Films', 'url' => ['action' => 'index']],
    ['title' => 'View', 'url' => ['action' => 'view', $film->id_film]],
    ['title' => 'Edit'],
]);
?>

<div class="card card-primary card-outline">
  <?= $this->Form->create($film) ?>
  <div class="card-body">
    <?php
      echo $this->Form->control('name');
      echo $this->Form->control('description');
      echo $this->Form->control('duration', ['empty' => true]);
      echo $this->Form->control('flag_id', ['options' => $flags, 'empty' => true]);
      echo $this->Form->control('type_id', ['options' => $types, 'empty' => true]);
    ?>
  </div>

  <div class="card-footer d-flex">
    <div class="">
      <?= $this->Form->postLink(
          __('Delete'),
          ['action' => 'delete', $film->id_film],
          ['confirm' => __('Are you sure you want to delete # {0}?', $film->id_film), 'class' => 'btn btn-danger']
      ) ?>
    </div>
    <div class="ml-auto">
      <?= $this->Form->button(__('Save')) ?>
      <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn btn-default']) ?>
    </div>
  </div>

  <?= $this->Form->end() ?>
</div>

