<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RentsFixture
 */
class RentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id_rent' => 1,
                'film_id' => 1,
                'rent_date' => 1678366188,
                'return_date' => 1678366188,
                'created' => 1678366188,
                'modified' => 1678366188,
            ],
        ];
        parent::init();
    }
}
