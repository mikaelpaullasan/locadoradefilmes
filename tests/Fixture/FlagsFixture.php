<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FlagsFixture
 */
class FlagsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id_flag' => 1,
                'flag' => 'Lorem ipsum dolor sit amet',
                'price' => 1,
                'created' => 1678366164,
                'modified' => 1678366164,
            ],
        ];
        parent::init();
    }
}
