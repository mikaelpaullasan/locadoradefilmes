<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TypesFixture
 */
class TypesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id_type' => 1,
                'nome' => 'Lorem ipsum dolor sit amet',
                'created' => 1678366195,
                'modified' => 1678366195,
            ],
        ];
        parent::init();
    }
}
