<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View;

use CakeLte\Style\Header;
use CakeLte\Style\Sidebar;
use Cake\View\View;
use CakeLte\View\CakeLteTrait;

/**
 * Application View
 *
 * Your application's default view class
 *
 * @link https://book.cakephp.org/4/en/views.html#the-app-view
 */
 class AppView extends View
{
    use CakeLteTrait;

    public $layout = 'CakeLte.default';

    public function initialize(): void{
        parent::initialize();
        $this->initializeCakeLte(
            $options = [
            'app-name' => '<b>Legendinha Filmes</b>',
            'app-logo' => 'pipoca.png',
    
            'small-text' => false,
            'dark-mode' => false,
            'layout-boxed' => false,
    
            'header' => [
                'fixed' => true,
                'border' => true,
                'style' => Header::STYLE_BLACK,
                'dropdown-legacy' => false,
            ],
    
            'sidebar' => [
                'fixed' => true,
                'collapsed' => true,
                'mini' => true,
                'mini-md' => false,
                'mini-xs' => false,
                'style' => Sidebar::STYLE_DARK_PRIMARY,
    
                'flat-style' => false,
                'legacy-style' => false,
                'compact' => true,
                'child-indent' => false,
                'child-hide-collapse' => false,
                'disabled-auto-expand' => false,
            ],
    
            'footer' => [
                'fixed' => false,
            ],
        ]
    );
        //...
    }
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading helpers.
     *
     * e.g. `$this->loadHelper('Html');`
     *
     * @return void
     */
}
