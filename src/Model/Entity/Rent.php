<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rent Entity
 *
 * @property int $id_rent
 * @property int|null $film_id
 * @property \Cake\I18n\FrozenTime|null $rent_date
 * @property \Cake\I18n\FrozenTime|null $return_date
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Film $film
 */
class Rent extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'film_id' => true,
        'rent_date' => true,
        'return_date' => true,
        'created' => true,
        'modified' => true,
        'film' => true,
    ];
}
