<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Film Entity
 *
 * @property int $id_film
 * @property string $name
 * @property string $description
 * @property \Cake\I18n\Time|null $duration
 * @property int|null $flag_id
 * @property int|null $type_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Flag $flag
 * @property \App\Model\Entity\Type $type
 * @property \App\Model\Entity\Rent[] $rents
 */
class Film extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'duration' => true,
        'flag_id' => true,
        'type_id' => true,
        'created' => true,
        'modified' => true,
        'flag' => true,
        'type' => true,
        'rents' => true,
    ];
}
